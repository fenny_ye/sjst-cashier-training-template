# 菜品管家系统

#### 介绍

大学时期与同学共同开发的项目

前端:React+AntDesign

后端:SpringBoot+Mybatis+MySQL+Thrift

#### 项目未合并到master分支

前端最新开发进度：[v2.4分支](https://gitee.com/fenny_ye/sjst-cashier-training-template/tree/feature/v2.4/ui/src/pages)

后端最新开发进度：[v2.5分支](https://gitee.com/fenny_ye/sjst-cashier-training-template/tree/feature%2Fv2.5/)


